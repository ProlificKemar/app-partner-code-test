//
//  APUser.h
//  IOSProgrammerTest
//
//  Created by Kemar White on 8/17/15.
//  Copyright (c) 2015 AppPartner. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APUser : NSObject

/**
 *  Username for the given user.
 */
@property (nonatomic, strong) NSString *username;

/**
 *  Corresponding password for the given user.
 */
@property (nonatomic, strong) NSString *password;

/**
 *  Determines if the login credientials for the user is valid.
 *
 *  @return Returns YES if the login credentials are valid, NO if they are not.
 */
- (BOOL)isValid;

@end
