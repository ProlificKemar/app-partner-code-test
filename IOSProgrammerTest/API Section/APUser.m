//
//  APUser.m
//  IOSProgrammerTest
//
//  Created by Kemar White on 8/17/15.
//  Copyright (c) 2015 AppPartner. All rights reserved.
//

#import "APUser.h"

@implementation APUser

#pragma mark - Setters

-(void)setPassword:(NSString *)password
{
    _password = (password.length > 3) ? password : nil;
}

#pragma mark - Public Methods

- (BOOL)isValid
{
    return (self.username && self.password);
}

@end
