//
//  LoginModel.h
//  IOSProgrammerTest
//
//  Created by Kemar White on 8/17/15.
//  Copyright (c) 2015 AppPartner. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface LoginModel : MTLModel <MTLJSONSerializing>

/**
 *  Error code (if one exists).
 */
@property (nonatomic, strong) NSString *code;

/**
 *  Message from server if login succeeded.
 */
@property (nonatomic, strong) NSString *message;

@end
