//
//  LoginModel.m
//  IOSProgrammerTest
//
//  Created by Kemar White on 8/17/15.
//  Copyright (c) 2015 AppPartner. All rights reserved.
//

#import "LoginModel.h"

@implementation LoginModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"code"    : @"code",
             @"message" : @"message"
             };
}

@end
