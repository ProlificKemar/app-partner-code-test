//
//  APISectionViewController.m
//  IOSProgrammerTest
//
//  Created by Justin LeClair on 12/15/14.
//  Copyright (c) 2014 AppPartner. All rights reserved.
//

#import "LoginSectionViewController.h"
#import "MainMenuViewController.h"

#import "APUser.h"
#import "LoginModel.h"
#import "NetworkingManager.h"

static NSString *const kPostEndPoint = @"AppPartnerProgrammerTest/scripts/login.php";

@interface LoginSectionViewController () <UITextFieldDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

@property (nonatomic, strong) APUser *user;

@end

@implementation LoginSectionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupDelegates];
    [self setupUI];
}

#pragma mark - Getters

- (APUser *)user
{
    if (!_user) {
        _user = [APUser new];
    }
    return _user;
}

#pragma mark - Private Methods

- (void)setupDelegates
{
    self.usernameTextField.delegate = self;
    self.passwordTextField.delegate = self;
}

- (void)setupUI
{
    [super setupUI];
    self.title = @"Login";
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_login"]];
}

- (void)userCredentialsFromTextField:(UITextField *)textField
{
    if (textField == self.usernameTextField) {
        self.user.username = textField.text;
        [self.passwordTextField becomeFirstResponder];

    } else if (textField == self.passwordTextField) {
        self.user.password = textField.text;
        [textField resignFirstResponder];
    }
}

#pragma mark - IBActions

- (IBAction)loginUser:(UIButton *)sender
{
    if (self.user.isValid) {
        [NetworkingManager loginUserWithEndPoint:kPostEndPoint :self.user completionHandler:^(LoginModel *loginResponse, NSError *error) {
            if ([loginResponse.code isEqual: @"Error"]) {
                NSLog(@"An error occurred - %@", loginResponse.message);
            } else if (error) {
                NSLog(@"An error occurred, - %@", error.localizedDescription);
            } else {
                NSLog(@"Login succeeded! - %@", loginResponse.message);
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Success!" message:@"You have sucessfully logged in!" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                [alertView show];
            }
        }];
    }
}

#pragma mark - UITextFieldDelegate Methods

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self userCredentialsFromTextField:textField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self userCredentialsFromTextField:textField];
    return YES;
}

#pragma mark - UIAlertViewDelegate Methods

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        MainMenuViewController *destinationVC = [[MainMenuViewController alloc] init];
        destinationVC.navigationItem.hidesBackButton = YES;
        [self.navigationController pushViewController:destinationVC animated:YES];
    }
}

@end
