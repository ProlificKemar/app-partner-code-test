//
//  AnimationSectionViewController.m
//  IOSProgrammerTest
//
//  Created by Justin LeClair on 12/15/14.
//  Copyright (c) 2014 AppPartner. All rights reserved.
//

#import "AnimationSectionViewController.h"
#import "MainMenuViewController.h"

#import "AnimationManager.h"
#import "DragableImageView.h"

@interface AnimationSectionViewController ()

@property (weak, nonatomic) IBOutlet DragableImageView *appIconImageView;

@end

@implementation AnimationSectionViewController

- (void)setupUI
{
    [super setupUI];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_animation"]];
    self.title = @"Animation";
}

#pragma mark - Private Methods

- (void)dragAppImageFromPanGesture:(UIPanGestureRecognizer *)gestureRecognizer
{
    CGRect frame = self.appIconImageView.frame;
    frame.origin = [gestureRecognizer locationInView:self.appIconImageView.superview];

    self.appIconImageView.frame = frame;
}

#pragma mark - IBActions

- (IBAction)spinIcon:(UIButton *)sender
{
    [_appIconImageView.layer addAnimation:[AnimationManager rotationAnimation] forKey:@"Spin"];
}

- (IBAction)backAction:(id)sender
{
    MainMenuViewController *mainMenuViewController = [[MainMenuViewController alloc] init];
    [self.navigationController pushViewController:mainMenuViewController animated:YES];
}

@end
