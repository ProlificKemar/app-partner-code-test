//
//  DragableImageView.h
//  IOSProgrammerTest
//
//  Created by Kemar White on 8/18/15.
//  Copyright (c) 2015 AppPartner. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DragableImageView : UIImageView

{
    /**
     *  Current point of the Imageview.
     */
    CGPoint currentPoint;
}

@end
