//
//  DragableImageView.m
//  IOSProgrammerTest
//
//  Created by Kemar White on 8/18/15.
//  Copyright (c) 2015 AppPartner. All rights reserved.
//

#import "DragableImageView.h"

@implementation DragableImageView

- (instancetype)initWithImage:(UIImage *)image
{
    if (self = [super initWithImage:image]) {
        self.userInteractionEnabled = YES;
    }
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    currentPoint = [[touches anyObject] locationInView:self];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGPoint imageCurrentPoint = [[touches anyObject] locationInView:self];
    CGPoint newMovePoint = CGPointMake(self.center.x + (imageCurrentPoint.x - currentPoint.x), self.center.y + (imageCurrentPoint.y - currentPoint.y));

    // Shift image to always keep within bounds of superview
    // First if is for when image is too far right else if is when image is too far left
    if (newMovePoint.x > CGRectGetWidth(self.superview.bounds) - CGRectGetMidX(self.bounds)) {
        newMovePoint.x = CGRectGetWidth(self.superview.bounds) - CGRectGetMidX(self.bounds);
    } else if (newMovePoint.x < CGRectGetMidX(self.bounds)) {
        newMovePoint.x = CGRectGetMidX(self.bounds);
    }
    // First if is for if image is too far down else if case is for when image is too far up
    if (newMovePoint.y > CGRectGetHeight(self.superview.bounds) - CGRectGetMidY(self.bounds)) {
        newMovePoint.y = CGRectGetHeight(self.superview.bounds) - CGRectGetMidY(self.bounds);
    } else if (newMovePoint.y < CGRectGetMidY(self.bounds)) {
        newMovePoint.y = CGRectGetMidY(self.bounds);
    }
    self.center = newMovePoint;
}

@end
