//
//  AnimationManager.h
//  IOSProgrammerTest
//
//  Created by Kemar White on 8/17/15.
//  Copyright (c) 2015 AppPartner. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CABasicAnimation;
@class UIImageView;

@interface AnimationManager : NSObject

/**
 *  Creates a basic rotation animation.
 *
 *  @return A single time rotation animation.
 */
+ (CABasicAnimation *)rotationAnimation;

@end
