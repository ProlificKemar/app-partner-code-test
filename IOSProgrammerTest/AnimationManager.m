
//
//  AnimationManager.m
//  IOSProgrammerTest
//
//  Created by Kemar White on 8/17/15.
//  Copyright (c) 2015 AppPartner. All rights reserved.
//

#import "AnimationManager.h"

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@implementation AnimationManager

+ (CABasicAnimation *)rotationAnimation
{
        CABasicAnimation *rotationAnimation;

        rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
        rotationAnimation.fromValue = [NSNumber numberWithFloat:0.0];
        rotationAnimation.toValue = [NSNumber numberWithFloat:(2 * M_PI)];
        rotationAnimation.duration = 1.1;
        rotationAnimation.repeatCount = 0;
        rotationAnimation.removedOnCompletion = NO;
        
        return rotationAnimation;
}

@end
