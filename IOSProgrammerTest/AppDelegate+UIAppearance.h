//
//  AppDelegate+UIAppearance.h
//  IOSProgrammerTest
//
//  Created by Kemar White on 8/17/15.
//  Copyright (c) 2015 AppPartner. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (UIAppearance)

- (void)setupAppearance;

@end
