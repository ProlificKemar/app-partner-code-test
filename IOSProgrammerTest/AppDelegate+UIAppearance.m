//
//  AppDelegate+UIAppearance.m
//  IOSProgrammerTest
//
//  Created by Kemar White on 8/17/15.
//  Copyright (c) 2015 AppPartner. All rights reserved.
//

#import "AppDelegate+UIAppearance.h"

@implementation AppDelegate (UIAppearance)

- (void)setupAppearance
{
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:44.0/255.0 green:69.0/255.0 blue:86.0/255.0 alpha:0.9]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
                                                           NSFontAttributeName: [UIFont fontWithName:@"Machinato" size:20.0]}];
}

@end
