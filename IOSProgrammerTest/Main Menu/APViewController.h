//
//  APViewController.h
//  IOSProgrammerTest
//
//  Created by Kemar White on 8/17/15.
//  Copyright (c) 2015 AppPartner. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APViewController : UIViewController

/**
 *  General UI setup used throughout the app.
 */
- (void)setupUI;

@end
