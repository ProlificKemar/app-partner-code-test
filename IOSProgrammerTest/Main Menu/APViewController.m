//
//  APViewController.m
//  IOSProgrammerTest
//
//  Created by Kemar White on 8/17/15.
//  Copyright (c) 2015 AppPartner. All rights reserved.
//

#import "APViewController.h"

@implementation APViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupUI];
}

- (void)setupUI
{
    UIImage *backButtonImage = [[UIImage imageNamed:@"btn_back"] resizableImageWithCapInsets:UIEdgeInsetsMake(0.0, 2.0, 0.0, 0.0)];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"" 
                                                                   style:UIBarButtonItemStylePlain 
                                                                  target:nil 
                                                                  action:nil];
    [backButton setBackButtonBackgroundImage:backButtonImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    self.navigationItem.backBarButtonItem = backButton;
}

@end
