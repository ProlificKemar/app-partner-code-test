//
//  NetworkingManager.h
//  IOSProgrammerTest
//
//  Created by Kemar White on 8/13/15.
//  Copyright (c) 2015 AppPartner. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LoginModel;
@class APUser;

@interface NetworkingManager : NSObject

/**
 *  Logs in a user at the given endpoint provided.
 *
 *  @param endpoint   Login endpoint.
 *  @param user       User to be logged in.
 *  @param completion Completion block.
 */
+ (void)loginUserWithEndPoint:(NSString *) endpoint :(APUser *)user 
            completionHandler:(void (^)(LoginModel *loginResponse, NSError *error) )completion;

@end
