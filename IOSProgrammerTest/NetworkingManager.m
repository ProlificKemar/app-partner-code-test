//
//  NetworkingManager.m
//  IOSProgrammerTest
//
//  Created by Kemar White on 8/13/15.
//  Copyright (c) 2015 AppPartner. All rights reserved.
//

#import "NetworkingManager.h"
#import "APUser.h"
#import "LoginModel.h"

#import <AFNetworking/AFNetworking.h>

static NSString *const kBaseURL = @"http://dev.apppartner.com/";

@implementation NetworkingManager

+ (void)loginUserWithEndPoint:(NSString *) endpoint :(APUser *)user 
            completionHandler:(void (^)(LoginModel *loginResponse, NSError *error) )completion
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *parameters = @{@"username" : user.username,
                                 @"password" : user.password
                                 };
    [manager POST:[NSString stringWithFormat:@"%@%@", kBaseURL, endpoint] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        // Need to get response time of how long API call took to return
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        LoginModel *loginResponse = [LoginModel new];
        loginResponse.code = JSON[@"code"];
        loginResponse.message = JSON[@"message"];
        completion(loginResponse, nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        completion(nil, error);
    }];
}

@end
