//
//  TableSectionTableViewCell.h
//  IOSProgrammerTest
//
//  Created by Justin LeClair on 12/15/14.
//  Copyright (c) 2014 AppPartner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatData.h"

@interface ChatCell : UITableViewCell

/**
 *  Loads cell with chat data information.
 *
 *  @param chatData Data to load into the cell.
 */
- (void)loadWithData:(ChatData *)chatData;

@end
