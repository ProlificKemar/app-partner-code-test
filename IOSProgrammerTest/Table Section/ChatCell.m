//
//  TableSectionTableViewCell.m
//  IOSProgrammerTest
//
//  Created by Justin LeClair on 12/15/14.
//  Copyright (c) 2014 AppPartner. All rights reserved.
//

#import "ChatCell.h"
#import "UIImage+Additions.h"

@interface ChatCell ()

@property (nonatomic, strong) IBOutlet UILabel     *usernameLabel;
@property (weak, nonatomic  ) IBOutlet UIImageView *avatarImageView;
@property (nonatomic, strong) IBOutlet UITextView  *messageTextView;

@end

@implementation ChatCell

- (void)loadWithData:(ChatData *)chatData
{
    self.usernameLabel.text = chatData.username;
    self.messageTextView.text = chatData.message;
    self.avatarImageView.image = [chatData.avatarImage add_imageWithRoundedBounds];
}

@end
