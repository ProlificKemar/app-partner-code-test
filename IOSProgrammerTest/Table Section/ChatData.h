//
//  ChatData.h
//  IOSProgrammerTest
//
//  Created by Justin LeClair on 12/19/14.
//  Copyright (c) 2014 AppPartner. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol ChatDataDelegate <NSObject>

- (void)chatDataDidReceiveAvatarImage:(BOOL)imageReceived;

@end

@interface ChatData : NSObject

@property (nonatomic, readwrite) int user_id;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) UIImage  *avatarImage;

/**
 *  Loads chat data from JSON dictionary.
 *
 *  @param dict The JSON dictionary of data.
 */
- (void)loadWithDictionary:(NSDictionary *)dict;

/**
 *  Sets up the delegate to be callbacked.
 *
 *  @param delegate The delegate.
 */
- (void)setupWithDelegate:(id)delegate;

@end
