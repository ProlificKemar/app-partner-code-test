//
//  ChatData.m
//  IOSProgrammerTest
//
//  Created by Justin LeClair on 12/19/14.
//  Copyright (c) 2014 AppPartner. All rights reserved.
//

#import "ChatData.h"

@interface ChatData ()

@property (nonatomic, strong) NSString *avatar_url;
@property (nonatomic, weak) id delegate;

@end

@implementation ChatData

- (void)loadWithDictionary:(NSDictionary *)dict
{
    self.user_id    = [[dict objectForKey:@"user_id"] intValue];
    self.username   = [dict objectForKey:@"username"];
    self.avatar_url = [dict objectForKey:@"avatar_url"];
    self.message    = [dict objectForKey:@"message"];
    dispatch_async(dispatch_get_main_queue(), ^{
        self.avatarImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.avatar_url]]];
        if ([self.delegate respondsToSelector:@selector(chatDataDidReceiveAvatarImage:)]) {
            [self.delegate chatDataDidReceiveAvatarImage:YES];
        }
    });
}

- (void)setupWithDelegate:(id)delegate
{
    self.delegate = delegate;
}

@end
